#include <iostream>
#include <conio.h> // 키를 입력하면 반응할 수 있게 해주는 헤더파일(_getch)
#include <time.h>

using namespace std;

/*
0 : 벽 
1 : 길
2 : 시작점
3 : 도착점
4 : 폭탄
5 : 파워아이템
6 : 벽밀기 아이템
7 : 투명아이템
8 : 웜홀
*/

// 플레이어의 위치를 만들기 위한 구조체
struct _tagPoint
{
	int x;
	int y;
};

//typedef : 타입을 재정의 하는 기능
typedef _tagPoint POINT;
typedef _tagPoint* PPOINT;

typedef struct _tagPlayer
{
	_tagPoint tPos;
	bool bWallPush;
	bool bPushOnOff;
	bool bTransparency;
	int iBombPower;
}PLAYER, * PPLAYER;

void SetMaze(char Maze[21][21], PPLAYER pPlayerPos, PPOINT pStartPos, PPOINT pEndPos)
{
	pStartPos->x = 0;
	pStartPos->y = 0;

	pEndPos->x = 19;
	pEndPos->y = 19;

	pPlayerPos->tPos = *pStartPos;

	strcpy_s(Maze[0], "21000000000000000000");
	strcpy_s(Maze[1], "01111111111100000000");
	strcpy_s(Maze[2], "01000000100000000000");
	strcpy_s(Maze[3], "01000000100000010000");
	strcpy_s(Maze[4], "01111110000000010000");
	strcpy_s(Maze[5], "01000010111101110000");
	strcpy_s(Maze[6], "01111111110110000000");
	strcpy_s(Maze[7], "00100000000100000000");
	strcpy_s(Maze[8], "00100000000100000000");
	strcpy_s(Maze[9], "00100000000111111100");
	strcpy_s(Maze[10], "01100000000000100100");
	strcpy_s(Maze[11], "01011110000000010100");
	strcpy_s(Maze[12], "01010010001111111100");
	strcpy_s(Maze[13], "01010010000100000000");
	strcpy_s(Maze[14], "01010010111111100000");
	strcpy_s(Maze[15], "01010010010000000000");
	strcpy_s(Maze[16], "01011010010000000100");
	strcpy_s(Maze[17], "01000010011110000100");
	strcpy_s(Maze[18], "01111110001000000000");
	strcpy_s(Maze[19], "00000001111111111113");
}

void Output(char Maze[21][21], PPLAYER pPlayer)
{
	for (int i = 0; i < 20; ++i)
	{
		for (int j = 0; j < 20; ++j)
		{
			if (Maze[i][j] == '4')
				cout << "※";
			else if (pPlayer->tPos.x == j && pPlayer->tPos.y == i)
				cout << "☆";
			else if (Maze[i][j] == '0')
				cout << "▩";
			else if (Maze[i][j] == '1') // 스페이스 두번 
				cout << "  ";
			else if (Maze[i][j] == '2')
				cout << "★";
			else if (Maze[i][j] == '3')
				cout << "◎";
			else if (Maze[i][j] == '5')
				cout << "♥";
			else if (Maze[i][j] == '6')
				cout << "♠";
			else if (Maze[i][j] == '7')
				cout << "♧";

		}
		cout << endl;
	}

	cout << "폭탄 파워 : " << pPlayer->iBombPower << endl;
	cout << "벽 통과 : ";
	if (pPlayer->bTransparency)
		cout << "ON\t";
	else
		cout << "OFF\t";
	cout << "벽 밀기 : ";
	if (pPlayer->bWallPush) // 아이템 + ON/OFF
	{
		cout << "가능 / ";

		if (pPlayer->bPushOnOff)
			cout << "ON" << endl;
		else
			cout << "OFF" << endl;
	}
	else
		cout << "불가능 / OFF" << endl;
}

bool AddItem(char cItemType, PPLAYER pPlayer)
{
	// 5 : 파워아이템		6 : 벽밀기 아이템			7 : 투명아이템  먹는 것
	if (cItemType == '5')
	{
		if(pPlayer->iBombPower < 5) // 폭탄 파워 MAXIMUM = 5
			++pPlayer->iBombPower;
		return true; // 아이템인 경우 true
	}
	else if (cItemType == '6')
	{
		pPlayer->bWallPush = true;
		pPlayer->bPushOnOff = true;
		return true;
	}
	else if (cItemType == '7')
	{
		pPlayer->bTransparency = true;
		return true;
	}

	return false; // 아이템이 아닐 경우 false
}

void MoveUp(char Maze[21][21], PPLAYER pPlayer)
{
	if (pPlayer->tPos.y - 1 >= 0) // 위로 이동했을 때 세로가 0보다 크거나 같아야 함
	{
		// 벽인지 체크 , 벽이라면 움직이지 않음
		if (Maze[pPlayer->tPos.y - 1][pPlayer->tPos.x] != '0' &&
			Maze[pPlayer->tPos.y - 1][pPlayer->tPos.x] != '4')
		{
			--pPlayer->tPos.y;
		}

		// 벽 밀기가 가능하고 바로 윗칸이 벽일 경우
		else if (pPlayer->bWallPush && Maze[pPlayer->tPos.y - 1][pPlayer->tPos.x] == '0')
		{
			// 벽 밀기가 On 상태일 경우
			if (pPlayer->bPushOnOff)
			{
				// 위의 위칸이 0보다 크거나 같을경우는 인덱스가 있다는 의미
				if (pPlayer->tPos.y - 2 >= 0)
				{
					// 위의 위칸이 길이어야 밀기가 가능하다. 그러므로 길인지 체크한다.
					// 길이 아닐 경우
					if (Maze[pPlayer->tPos.y - 2][pPlayer->tPos.x] == '0')
					{
						if (pPlayer->bTransparency)
							--pPlayer->tPos.y;
					}
					// 길일 경우 
					else if (Maze[pPlayer->tPos.y - 2][pPlayer->tPos.x] == '1')
					{
						// 위의 위칸을 벽으로 하고 
						Maze[pPlayer->tPos.y - 2][pPlayer->tPos.x] = '0';
						// 위칸은 벽이었는데 길로 만들어준다.
						Maze[pPlayer->tPos.y - 1][pPlayer->tPos.x] = '1';
						// 플레이어를 이동시킨다.
						--pPlayer->tPos.y;
					}
				}

				else if (pPlayer->bTransparency)
					--pPlayer->tPos.y;
			}

			// 벽 밀기 OFF상태일 경우
			else if (pPlayer->bTransparency)
				--pPlayer->tPos.y;
		}

		// 투명화 상태
		else if (pPlayer->bTransparency)
			--pPlayer->tPos.y;

		// 아이템인 경우 습득 하고 길로 만든다. 
		if (AddItem(Maze[pPlayer->tPos.y][pPlayer->tPos.x], pPlayer))
			Maze[pPlayer->tPos.y][pPlayer->tPos.x] = '1';

		
	}
}

void MoveDown(char Maze[21][21], PPLAYER pPlayer)
{
	if (pPlayer->tPos.y + 1 < 20) // 아래로 한칸 이동했을 때 마지막보다는 작아야함
	{
		// 벽인지 체크
		if (Maze[pPlayer->tPos.y + 1][pPlayer->tPos.x] != '0' &&
			Maze[pPlayer->tPos.y + 1][pPlayer->tPos.x] != '4')
		{
			++pPlayer->tPos.y;
		}

		// 벽 밀기가 가능하고 바로 윗칸이 벽일 경우
		else if (pPlayer->bWallPush && Maze[pPlayer->tPos.y + 1][pPlayer->tPos.x] == '0')
		{
			// 벽 밀기가 On 상태일 경우
			if (pPlayer->bPushOnOff)
			{
				// 위의 위칸이 0보다 크거나 같을경우는 인덱스가 있다는 의미
				if (pPlayer->tPos.y + 2 < 20)
				{
					// 위의 위칸이 길이어야 밀기가 가능하다. 그러므로 길인지 체크한다.
					// 길이 아닐 경우
					if (Maze[pPlayer->tPos.y + 2][pPlayer->tPos.x] == '0')
					{
						if (pPlayer->bTransparency)
							++pPlayer->tPos.y;
					}
					// 길일 경우 
					else if (Maze[pPlayer->tPos.y + 2][pPlayer->tPos.x] == '1')
					{
						// 위의 위칸을 벽으로 하고 
						Maze[pPlayer->tPos.y + 2][pPlayer->tPos.x] = '0';
						// 위칸은 벽이었는데 길로 만들어준다.
						Maze[pPlayer->tPos.y + 1][pPlayer->tPos.x] = '1';
						// 플레이어를 이동시킨다.
						++pPlayer->tPos.y;
					}
				}
				else if (pPlayer->bTransparency)
					++pPlayer->tPos.y;
			}
			// 벽 밀기 OFF상태일 경우
			else if (pPlayer->bTransparency)
				++pPlayer->tPos.y;
		}

		// 투명화 상태
		else if (pPlayer->bTransparency)
			++pPlayer->tPos.y;

		// 아이템인 경우 습득 하고 길로 만든다. 
		if (AddItem(Maze[pPlayer->tPos.y][pPlayer->tPos.x], pPlayer))
			Maze[pPlayer->tPos.y][pPlayer->tPos.x] = '1';

	}
}

void MoveRight(char Maze[21][21], PPLAYER pPlayer)
{
	if (pPlayer->tPos.x + 1 < 20)
	{
		// 벽인지 체크
		if (Maze[pPlayer->tPos.y][pPlayer->tPos.x + 1] != '0' &&
			Maze[pPlayer->tPos.y][pPlayer->tPos.x + 1] != '4')
		{
			++pPlayer->tPos.x;
		}
		// 벽 밀기가 가능하고 바로 윗칸이 벽일 경우
		else if (pPlayer->bWallPush && Maze[pPlayer->tPos.y][pPlayer->tPos.x + 1] == '0')
		{
			// 벽 밀기가 On 상태일 경우
			if (pPlayer->bPushOnOff)
			{
				// 위의 위칸이 0보다 크거나 같을경우는 인덱스가 있다는 의미
				if (pPlayer->tPos.x + 2 < 20)
				{
					// 위의 위칸이 길이어야 밀기가 가능하다. 그러므로 길인지 체크한다.
					// 길이 아닐 경우
					if (Maze[pPlayer->tPos.y][pPlayer->tPos.x + 2] == '0')
					{
						if (pPlayer->bTransparency)
							++pPlayer->tPos.x;
					}
					// 길일 경우 
					else if (Maze[pPlayer->tPos.y][pPlayer->tPos.x + 2] == '1')
					{
						// 위의 위칸을 벽으로 하고 
						Maze[pPlayer->tPos.y][pPlayer->tPos.x + 2] = '0';
						// 위칸은 벽이었는데 길로 만들어준다.
						Maze[pPlayer->tPos.y][pPlayer->tPos.x + 1] = '1';
						// 플레이어를 이동시킨다.
						++pPlayer->tPos.x;
					}
				}
				else if (pPlayer->bTransparency)
					++pPlayer->tPos.x;
			}
			// 벽 밀기 OFF상태일 경우
			else if (pPlayer->bTransparency)
				++pPlayer->tPos.x;
		}
		// 투명화 상태
		else if (pPlayer->bTransparency)
			++pPlayer->tPos.x;

		// 아이템인 경우 습득 하고 길로 만든다. 
		if (AddItem(Maze[pPlayer->tPos.y][pPlayer->tPos.x], pPlayer))
			Maze[pPlayer->tPos.y][pPlayer->tPos.x] = '1';

	}
}

void MoveLeft(char Maze[21][21], PPLAYER pPlayer)
{
	if (pPlayer->tPos.x - 1 >= 0)
	{
		// 벽인지 체크
		if (Maze[pPlayer->tPos.y][pPlayer->tPos.x - 1] != '0' &&
			Maze[pPlayer->tPos.y][pPlayer->tPos.x - 1] != '4')
		{
			--pPlayer->tPos.x;
		}
		// 벽 밀기가 가능하고 바로 윗칸이 벽일 경우
		else if (pPlayer->bWallPush && Maze[pPlayer->tPos.y][pPlayer->tPos.x - 1] == '0')
		{
			// 벽 밀기가 On 상태일 경우
			if (pPlayer->bPushOnOff)
			{
				// 위의 위칸이 0보다 크거나 같을경우는 인덱스가 있다는 의미
				if (pPlayer->tPos.x - 2 >= 0)
				{
					// 위의 위칸이 길이어야 밀기가 가능하다. 그러므로 길인지 체크한다.
					// 길이 아닐 경우
					if (Maze[pPlayer->tPos.y][pPlayer->tPos.x - 2] == '0')
					{
						if (pPlayer->bTransparency)
							--pPlayer->tPos.x;
					}
					// 길일 경우 
					else if (Maze[pPlayer->tPos.y][pPlayer->tPos.x - 2] == '1')
					{
						// 위의 위칸을 벽으로 하고 
						Maze[pPlayer->tPos.y][pPlayer->tPos.x - 2] = '0';
						// 위칸은 벽이었는데 길로 만들어준다.
						Maze[pPlayer->tPos.y][pPlayer->tPos.x - 1] = '1';
						// 플레이어를 이동시킨다.
						--pPlayer->tPos.x;
					} 
				}
				else if (pPlayer->bTransparency)
					--pPlayer->tPos.x;
			}
			// 벽 밀기 OFF상태일 경우
			else if (pPlayer->bTransparency)
				--pPlayer->tPos.x;
		}
		// 투명화 상태
		else if (pPlayer->bTransparency)
			--pPlayer->tPos.x;
	
		// 아이템인 경우 습득 하고 길로 만든다. 
		if (AddItem(Maze[pPlayer->tPos.y][pPlayer->tPos.x], pPlayer))
			Maze[pPlayer->tPos.y][pPlayer->tPos.x] = '1';

	}
}

// 움직이는 기능
void MovePlayer(char Maze[21][21], PPLAYER pPlayer, char cInput)
{
	switch (cInput)
	{
	case 'w':
	case 'W':
		MoveUp(Maze, pPlayer);
		break;
	case 'a':
	case 'A': 
		MoveLeft(Maze, pPlayer);
		break;
	case 's':
	case 'S':
		MoveDown(Maze, pPlayer);
		break;
	case 'd':
	case 'D':
		MoveRight(Maze, pPlayer);
		break;
	}

}

// 포인터 변수를 const로 생성하면 가리키는 대상의 값을 변경할 수 없다.
void CreateBomb(char Maze[21][21], const PPLAYER pPlayer, PPOINT pBombArr, 
	int *pBombCount)
{
	if (*pBombCount == 5)
		return;
	// 투명화 상태에서 벽 위에 있을 경우 폭탄 생성 불가
	else if (Maze[pPlayer->tPos.y][pPlayer->tPos.x] == '0')
		return;

	// 중복위치에 폭탄설치가 안되게 한다.
	for (int i = 0; i < *pBombCount; ++i)
	{
		if (pPlayer->tPos.x == pBombArr[i].x && pPlayer->tPos.y == pBombArr[i].y)
			return;
	}

	pBombArr[*pBombCount] = pPlayer->tPos;
	++(*pBombCount);

	Maze[pPlayer->tPos.y][pPlayer->tPos.x] = '4'; //4 = 폭탄
}

// 폭탄을 중심으로 위,아래,좌,우 네칸을 폭파
void Fire(char Maze[21][21], PPLAYER pPlayer, PPOINT pBombArr, int* pBombCount)
{
	for (int i = 0; i < *pBombCount; ++i)
	{
		Maze[pBombArr[i].y][pBombArr[i].x] = '1';

		// 플레이어가 폭탄에 맞았을 때 시작점으로 보낸다.
		if (pPlayer->tPos.x == pBombArr[i].x && pPlayer->tPos.y == pBombArr[i].y)
		{
			pPlayer->tPos.x = 0;
			pPlayer->tPos.y = 0;
		}

		for (int j = 1; j <= pPlayer->iBombPower; ++j)
		{
			// 위
			if (pBombArr[i].y - j >= 0)
			{
				// 아이템 드랍 확률을 구한다. 벽 파괴시 20%확률로 랜덤한 아이템 등장
				if (rand() % 100 < 20)
				{
					int iPercent = rand() % 100;
					if (iPercent < 40)
						Maze[pBombArr[i].y - j][pBombArr[i].x] = '5';
					else if (iPercent < 70)
						Maze[pBombArr[i].y - j][pBombArr[i].x] = '6';
					else 
						Maze[pBombArr[i].y - j][pBombArr[i].x] = '7';
				}
				// 폭탄이 터졌을 때 벽이 사라짐
				else 
					Maze[pBombArr[i].y - j][pBombArr[i].x] = '1';

				// 플레이어가 폭탄에 맞았을 때 시작점으로 보낸다.
				if (pPlayer->tPos.x == pBombArr[i].x && pPlayer->tPos.y == pBombArr[i].y - j)
				{
					pPlayer->tPos.x = 0;
					pPlayer->tPos.y = 0;
				}
			}
			// 아래
			if (pBombArr[i].y + j < 20)
			{
				// 아이템 드랍 확률을 구한다. 벽 파괴시 20%확률로 랜덤한 아이템 등장
				if (rand() % 100 < 20)
				{
					int iPercent = rand() % 100;
					if (iPercent < 40)
						Maze[pBombArr[i].y + j][pBombArr[i].x] = '5';
					else if (iPercent < 70)	   
						Maze[pBombArr[i].y + j][pBombArr[i].x] = '6';
					else							   
						Maze[pBombArr[i].y + j][pBombArr[i].x] = '7';
				}
				// 폭탄이 터졌을 때 벽이 사라짐
				else
					Maze[pBombArr[i].y + j][pBombArr[i].x] = '1';

				// 플레이어가 폭탄에 맞았을 때 시작점으로 보낸다.
				if (pPlayer->tPos.x == pBombArr[i].x && pPlayer->tPos.y == pBombArr[i].y + j)
				{
					pPlayer->tPos.x = 0;
					pPlayer->tPos.y = 0;
				}
			}
			// 왼쪽
			if (pBombArr[i].x - j >= 0)
			{
				// 아이템 드랍 확률을 구한다. 벽 파괴시 20%확률로 랜덤한 아이템 등장
				if (rand() % 100 < 20)
				{
					int iPercent = rand() % 100;
					if (iPercent < 40)
						Maze[pBombArr[i].y][pBombArr[i].x - j] = '5';
					else if (iPercent < 70)
						Maze[pBombArr[i].y][pBombArr[i].x - j] = '6';
					else
						Maze[pBombArr[i].y][pBombArr[i].x - j] = '7';
				}
				// 폭탄이 터졌을 때 벽이 사라짐
				else
					Maze[pBombArr[i].y][pBombArr[i].x - j] = '1';

				// 플레이어가 폭탄에 맞았을 때 시작점으로 보낸다.
				if (pPlayer->tPos.x == pBombArr[i].x - j && pPlayer->tPos.y == pBombArr[i].y)
				{
					pPlayer->tPos.x = 0;
					pPlayer->tPos.y = 0;
				}
			}
			// 오른쪽
			if (pBombArr[i].x + j < 20)
			{
				// 아이템 드랍 확률을 구한다. 벽 파괴시 20%확률로 랜덤한 아이템 등장
				if (rand() % 100 < 20)
				{
					int iPercent = rand() % 100;
					if (iPercent < 40)
						Maze[pBombArr[i].y][pBombArr[i].x + j] = '5';
					else if (iPercent < 70)							 
						Maze[pBombArr[i].y][pBombArr[i].x + j] = '6';
					else													   	 
						Maze[pBombArr[i].y][pBombArr[i].x + j] = '7';
				}
				// 폭탄이 터졌을 때 벽이 사라짐
				else
					Maze[pBombArr[i].y][pBombArr[i].x + j] = '1';

				// 플레이어가 폭탄에 맞았을 때 시작점으로 보낸다.
				if (pPlayer->tPos.x == pBombArr[i].x + j && pPlayer->tPos.y == pBombArr[i].y)
				{
					pPlayer->tPos.x = 0;
					pPlayer->tPos.y = 0;
				}
			}
		}
	}
	*pBombCount = 0;
}

int main()
{
	srand((unsigned int)time(0));

	// 20 x 20 미로를 만들어 준다. 마지막 Null을 위해 21로 설정
	char strMaze[21][21] = {};

	PLAYER tPlayer = {};
	POINT tStartPos;
	POINT tEndPos;

	tPlayer.iBombPower = 1;

	int iBombCount = 0; // 전체
	 
	POINT tBombPos[5];

	// 미로를 설정한다.
	SetMaze(strMaze, &tPlayer, &tStartPos, &tEndPos);

	while (true)
	{
		system("cls");
		// 미로를 출력한다.
		Output(strMaze, &tPlayer);

		// 도착점에 도달했을 경우
		if (tPlayer.tPos.x == tEndPos.x && tPlayer.tPos.y == tEndPos.y)
		{
			cout << "도착했습니다." << endl;
			break;
		}

		cout << "T : 폭탄 설치 U : 폭탄 터트리기 I : 벽밀기 ON/OFF" << endl; 

		cout << "W : 위 , S : 아래, A : 왼쪽 , D : 오른쪽 , Q : 종료 : ";
		char cInput = _getch(); // 입력되면 바로 반응

		if (cInput == 'q' || cInput == 'Q')
			break;
		else if (cInput == 't' || cInput == 'T') // 폭탄도 장애물로 인식해야된다.
			CreateBomb(strMaze, &tPlayer, tBombPos, &iBombCount);

		else if (cInput == 'u' || cInput == 'U')
			Fire(strMaze, &tPlayer, tBombPos, &iBombCount);

		else if (cInput == 'i' || cInput == 'I')
		{
			if (tPlayer.bWallPush)
				tPlayer.bPushOnOff = !tPlayer.bPushOnOff;
		}

		else	
			MovePlayer(strMaze, &tPlayer, cInput);
	}

	
	return 0; 
}