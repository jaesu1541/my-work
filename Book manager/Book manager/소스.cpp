#include <iostream>




using namespace std;

#define BOOK_SIZE 32
#define BOOK_NAME 50
#define BOOK_MAX 10 
#define NAME_SIZE 50

struct _tagBook
{
	char strName[BOOK_SIZE];
	int iPrice;
	int iNumber;
	bool iBorrow;
};

enum MENU
{
	MENU_NONE,
	MENU_REGISTER,
	MENU_BORROW,
	MENU_RETURN,
	MENU_LIST,
	MENU_EXIT
};

int main()
{
	_tagBook tBookArr[BOOK_MAX] = {};
	int iBookCount = 0;
	int iBookNumber = 1;

	char strName[NAME_SIZE] = {};


	while (true)
	{
		system("cls");
		
		// 메뉴 출력 
		cout << "========== 메뉴 ==========" << endl;
		cout << "1. 책 등록" << endl;
		cout << "2. 책 대여" << endl;
		cout << "3. 책 반납" << endl;
		cout << "4. 책 목록" << endl;
		cout << "5. 종료" << endl;

		cout << "메뉴를 선택하세요 : ";
		int iMenu;
		cin >> iMenu;

		if (cin.fail())
		{
			cin.clear();
			cin.ignore(1024, '\n');
			continue;
		}

		if (iMenu == MENU_EXIT)
			break;

		switch (iMenu)
		{
		case MENU_REGISTER:
			system("cls");
			cout << "========== 책 정보 추가  ==========" << endl << endl;

			if (iBookCount == BOOK_MAX)
			{
				cout << "더 이상 책을 등록할 수 없습니다." << endl;
				break;
			}

			cin.ignore(1024, '\n');

			cout << "책 이름 : ";
			cin.getline(tBookArr[iBookCount].strName, BOOK_NAME);
			cout << "대여 금액 : ";
			cin >> tBookArr[iBookCount].iPrice;

			tBookArr[iBookCount].iBorrow = false;

			tBookArr[iBookCount].iNumber = iBookNumber;

			cout << "책 등록 완료" << endl;

			++iBookNumber;
			++iBookCount;	
			
			break;
		case MENU_BORROW:
			system("cls");

			cout << "========== 책 대여  ==========" << endl << endl;
			cin.ignore(1024, '\n');
			cout << "대여할 책 이름을 입력하세요 : ";
			cin.getline(strName, NAME_SIZE);

			for (int i = 0; i < iBookCount; ++i)
			{
				if (strcmp(strName, tBookArr[i].strName) == 0)
				{
					if (tBookArr[i].iBorrow == false)
					{
						cout << tBookArr[i].strName << "을(를) 대여했습니다." << endl;
						tBookArr[i].iBorrow = true;
						break;
					}
					
					else if (tBookArr[i].iBorrow == true)
					{
						cout << "책이 대여중입니다." << endl;
						break;
					}
					
				}
				else if ((i  + 1 == iBookNumber) && strcmp(strName, tBookArr[i].strName) != 0)
				{
					cout << "책 목록에 없습니다." << endl;
					break;
				}
			}
			break;
		case MENU_RETURN:
			system("cls");
			cout << "========== 책 반납  ==========" << endl << endl;

			cin.ignore(1024, '\n');
			cout << "반납할 책 이름을 입력해주세요 : ";
			cin.getline(strName, NAME_SIZE);

			for (int i = 0; i < iBookCount; ++i)
			{
				if (strcmp(strName, tBookArr[i].strName) == 0)
				{
					if (tBookArr[i].iBorrow == false)
					{
						cout << "대여를 하지 않음" << endl;
						break;
					}
					tBookArr[i].iBorrow = false;
					cout << "반납이 완료되었습니다." << endl;
					break;
				}
				else if ((i + 1 == iBookNumber) && strcmp(strName, tBookArr[i].strName) != 0)
				{
					cout << "잘못된 정보를 입력하였습니다." << endl;
					break;
				}
			}
			break;
		case MENU_LIST:
			system("cls");
			cout << "========== 책 목록  ==========" << endl << endl;

			for (int i = 0; i < iBookCount; ++i)
			{
				cout << "책 이름 : " << tBookArr[i].strName << endl;
				cout << "책 번호 : " << tBookArr[i].iNumber << endl;
				cout << "책 대여료 : " << tBookArr[i].iPrice << endl;
				if (tBookArr[i].iBorrow == false)
					cout << "책 대여 여부 : 가능" << endl << endl;
				else
					cout << "책 대여 여부 : 불가능" << endl << endl;
			}

			break;
		
		default:
			cout << "잘못 선택했습니다." << endl;
			break;
		}
		system("pause");
	}
	return 0;
}